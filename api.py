from flask import Flask, request
from flask_pymongo import PyMongo
import re
from dotenv import load_dotenv
import os
from pymongo import MongoClient

load_dotenv()

MONGO_URL = os.environ.get("MONGO_URL", "localhost")
client = MongoClient(MONGO_URL)
db = client.bmatdb


app = Flask(__name__)
app.config["MONGO_URI"] = MONGO_URL
mongo = PyMongo(app)


# To get the right_owners related to one specific ISWC, use the arg ?iswc in the URL
@app.route('/', methods=['GET', 'POST'])
def get_by_iswc():
    iswc = request.args.get('iswc')
    if not iswc:
        return {"error": "Missing ISWC parameter"}, 400
    col = db.sr
    sr = col.find_one({"iswc": re.sub(r'[^\w\s]', '', iswc).strip()})
    if sr:
        return {"right_owners": sr["right_owners"]}
    else:
        return {"error": "ISWC not found"}, 404


if __name__ == '__main__':
    app.run(debug=True)
