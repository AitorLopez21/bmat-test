import csv
import re
import argparse
from dotenv import load_dotenv
import os
from pymongo import MongoClient


def titles_parser(row):
    titles = []
    # For every type of title (e.g. AlternativeTitle1),if the row
    #  includes this information we add it to our titles array
    titles.append({
        "name": row["ORIGINAL TITLE"],
        "type": "OriginalTitle"
    })
    if row["ALTERNATIVE TITLE 1"].strip():
        titles.append({
            "name": row["ALTERNATIVE TITLE 1"],
            "type": "AlternativeTitle1"
        })
    if row[" ALTERNATIVE TITLE 2"].strip():
        titles.append({
            "name": row[" ALTERNATIVE TITLE 2"],
            "type": "AlternativeTitle2"
        })
    if row["ALTERNATIVE TITLE 3"].strip():
        titles.append({
            "name": row["ALTERNATIVE TITLE 3"],
            "type": "AlternativeTitle3"
        })
    return titles


def owners_appender(row, current_owners):
    current_owners.append({
        "name": row["RIGHT OWNER"],
        "role": row["ROLE"],
        # Using zfill for the zero padded to the left (11 characters)
        # If empty it will result in 11 zeros
        "ipi": row["IPI NUMBER"].zfill(11)
    })
    return current_owners


def init_db(args):
    # Resets sr db collection if needed
    if args.resetdb:
        db.sr.drop()


def parse_csv(input_file):
    data = []
    # Open a csv reader called DictReader
    with open(input_file, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        old_id = ""

        for row in csvReader:
            # If the old_id is the same one as the row before,
            # we just take the owner information and add it
            if old_id == row["ID SOCIETY"]:
                owners_appender(row, data[-1]["right_owners"])
            # Otherwise, we create a new dict and fill it with all the data
            else:
                # We buffer up to 100 works and then bulk insert them
                if len(data) > 100:
                    insert_to_mongo(data)
                    data = []

                to_insert = {}
                new_owners = []

                titles = titles_parser(row)
                owners = owners_appender(row, new_owners)
                to_insert["_id"] = row["ID SOCIETY"]
                # We remove any possible punctuation from ISWC codes
                to_insert["iswc"] = re.sub(r'[^\w\s]', '', row["ISWC"])
                to_insert["titles"] = titles
                to_insert["right_owners"] = owners
                data.append(to_insert)
                # Update the old_id variable with the processed row ID SOCIETY
                old_id = row["ID SOCIETY"]
        if data:
            insert_to_mongo(data)


def insert_to_mongo(data):
    db.sr.insert_many(data)


if __name__ == '__main__':
    load_dotenv()

    MONGO_URL = os.environ.get("MONGO_URL", "localhost")
    client = MongoClient(MONGO_URL)
    db = client.bmatdb
    parser = argparse.ArgumentParser()
    parser.add_argument("--input-file", required=True,
                        help="csv file to insert in DB")
    parser.add_argument("--resetdb", action="store_true", required=False,
                        help="resets sr DB collection")
    args = parser.parse_args()
    init_db(args)
    parse_csv(args.input_file)
    # we create a ISWC index to optimize searches for our clients.
    # If the index is already created it has no effect

    db.sr.create_index([('iswc', 1)])
