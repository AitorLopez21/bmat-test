# BMAT TEST

_Quick instructions to execute the BMAT test_

_In order to facilitate the execution of the test, I deployed the mongoDB in Atlas. 
A connection string is provided in the email_

## Steps to execute the parser

1. Create a virtual env with python 3.7+ and activate it

2. Install the requirements

```
pip install -r requirements.txt
```

3. Set up .env file with the connection string (available with the email). A .env-template is provided with the variable to set up

4. Execute the parser.py

```
python parser.py --db_works_test.csv
```

## Steps to execute the API

1. Execute api.py file

```
python api.py
```
2. Now you can access the URL provided in the terminal to use the API. In order to consult
about a specific ISWC use the parameter _?iswc_ 

An example would be:

```
http://127.0.0.1:5000/?iswc=T0421424954
```


## Steps to execute the unit test

1. Install the requirements-dev.txt

```
pip install -r requirements-dev.txt
```

2. Execute the test.py, test_file.csv is provided

```
python test.py
```
