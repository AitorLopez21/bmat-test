import unittest
import mock

from parser import parse_csv

tested_output = [{'_id': '611321', 'iswc': 'T0421644792', 'titles':
                  [{'name': 'CHA CHA CHA DE BAHIA', 'type': 'OriginalTitle'},
                   {'name': 'CHACHACHA EN BAHIA', 'type': 'AlternativeTitle1'},
                   {'name': 'CHA CHACHA EN BAHÍA', 'type': 'AlternativeTitle2'},
                   {'name': 'CHA CHACHA EN BAHIA', 'type': 'AlternativeTitle3'}],
                  'right_owners':
                  [{'name': 'ENRIQUE JESUS JORRIN Y OLEAGA', 'role':
                    'Compositor/Autor', 'ipi': '00015546498'},
                   {'name': 'EDMOND DAVID BACRI', 'role':
                    'Adaptador', 'ipi': '00001772516'}]}]


class TestParser(unittest.TestCase):
    """
    We test the most complete example (multiple titles, right owners).
    In a production enviorement we would do specific unit test for each function
    """
    @mock.patch("parser.insert_to_mongo")
    def test_parser(self, mock_insert_to_mongo):
        parse_csv("test_file.csv")
        result = mock_insert_to_mongo.call_args.args[0]
        self.assertEqual(result, tested_output)


if __name__ == '__main__':
    unittest.main()
